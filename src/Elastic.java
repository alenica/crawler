import java.io.IOException;
import java.util.Map;
import org.apache.http.HttpHost;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RestClient;
import  org.elasticsearch.rest.RestStatus;
import org.elasticsearch.client.RestHighLevelClient;


public class Elastic implements AutoCloseable{
    public RestStatus requestStatus;
    static RestHighLevelClient client = new RestHighLevelClient(
            RestClient.builder(
                    new HttpHost("localhost", 9200, "http")));

    public void RequestToPut (Map<String, Object> jsonMap, String index, String type, String id) {
        IndexRequest indexRequest = new IndexRequest(index, type, id)
                .source(jsonMap);
        IndexResponse indexResponse = null;
        try {
            indexResponse = client.index(indexRequest);
        } catch (IOException e) {
            System.out.println(e);
        } finally {
            this.requestStatus = indexResponse.status();
        }
    }
    public void close() throws IOException {
        client.close();
    }
}
