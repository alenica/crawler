import org.apache.http.HttpEntity;
import org.apache.http.client.fluent.Request;

import org.apache.http.client.fluent.Content;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import org.json.XML;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class OpenArchive {

    public CloseableHttpClient httpclient;
    public Content response;

    public OpenArchive() throws IOException {
        httpclient = HttpClients.createDefault();
    }
    //получаем данные по url
    public Map<String, Object> get(String url) throws IOException {
        response = Request.Get(url).execute().returnContent();
        Map<String, Object> jsonMap = parseXMLtoJson(response.asString());
        return jsonMap;
    }
    //функция парсилка
    private static Map<String, Object> parseXMLtoJson(String xml) {
        JSONObject json = XML.toJSONObject(xml);
        Map<String, Object> jsonMap = new HashMap<String, Object>();

        if (json.getJSONObject("OAI-PMH").has("error")) {
            String error = json.getJSONObject("OAI-PMH").getJSONObject("error").get("code").toString();
            jsonMap.put("error", error);
            return jsonMap;

        } else {
            Map<String, Object> clearJson = new HashMap<String, Object>();

            String identifier = json.getJSONObject("OAI-PMH").getJSONObject("GetRecord").getJSONObject("record")
                    .getJSONObject("header").get("identifier")
                    .toString();
            String id = identifier.substring(identifier.length() - 7);
            clearJson.put("id", id);

            JSONObject metadata = json.getJSONObject("OAI-PMH").getJSONObject("GetRecord").getJSONObject("record")
                    .getJSONObject("metadata").getJSONObject("oai_dc:dc");

            clearJson.put("description", metadata.get("dc:description"));
            clearJson.put("type", metadata.get("dc:type"));
            clearJson.put("subject", metadata.get("dc:subject"));
            clearJson.put("title", metadata.get("dc:title"));
            clearJson.put("date", metadata.get("dc:date"));
            clearJson.put("creator", metadata.get("dc:creator"));
            return clearJson;
        }

    }
}
