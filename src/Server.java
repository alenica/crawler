import com.rabbitmq.client.*;
import org.json.JSONObject;
import org.elasticsearch.rest.RestStatus;

import java.io.IOException;
import java.util.Map;

public class Server {
    private static final String QUEUE_NAME = "Request Queue";

    //забирает данные из архива
    private static Map<String, Object> getData(String url) throws IOException {
        OpenArchive archive = new OpenArchive();
        return archive.get(url);
    }

    //отправляет данные в эластик, возвращает статус отправки
    private static RestStatus putDataToElasticsearchAndGetStatus(Map<String, Object> data) throws IOException {
        Elastic elastic = new Elastic();
        try {
            elastic.RequestToPut(data, "articles", "doc", data.get("id").toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return elastic.requestStatus;
        }
    }

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            channel.queuePurge(QUEUE_NAME);

            channel.basicQos(1);

            System.out.println(" [x] Awaiting requests");

            Object monitor = new Object();
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                        .Builder()
                        .correlationId(delivery.getProperties().getCorrelationId())
                        .build();
                String response = "";
                //пытается достучаться до очереди поймать оттуда ссылку
                try {
                    String url = new String(delivery.getBody(), "UTF-8");
                    System.out.println("[x] Recieve url: " + url);
                    //получает данные из архива
                    Map<String, Object> data = getData(url);
                    //если данные корректные и если успешно загрузились в эластик, возвращаем в очередь ОК
                    if (!data.containsKey("error") && (putDataToElasticsearchAndGetStatus(data) == RestStatus.OK)) {
                        response = "OK";
                    } else {
                        response = "Error";
                    }
                } catch (RuntimeException e) {
                    e.printStackTrace();
                } finally {
                    //возвращаем в очередь response
                    channel.basicPublish("", delivery.getProperties().getReplyTo(), replyProps, response.getBytes("UTF-8"));
                    channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                    // RabbitMq consumer worker thread notifies the RPC server owner thread
                    synchronized (monitor) {
                        monitor.notify();
                    }
                }
            };

            channel.basicConsume(QUEUE_NAME, false, deliverCallback, (consumerTag -> {
            }));
            //слушаем очереди
            while (true) {
                synchronized (monitor) {
                    try {
                        monitor.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }


}