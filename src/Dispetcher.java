import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Dispetcher {
    private static String makeURL(String site, String verb, String identifier, String metadataPrefix) {
        return (site + "?verb=" + verb + "&identifier=" + identifier + "&metadataPrefix=" + metadataPrefix);
    }
    public static void main(String[] argv) {

        try (Client client = new Client()) {
            String request = makeURL("http://arXiv.org/oai2", "GetRecord", "oai:arXiv.org:cs/0112016",
                    "oai_dc");
            System.out.println(" [x] Requesting " + request);
            String response = client.call(request);
            System.out.println(" [.] Got '" + response + "'");
        } catch (IOException | TimeoutException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}

//curl -X GET "localhost:9200/articles/doc/0112016?pretty&_source=title"